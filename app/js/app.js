'use strict';
angular.module('zeissApp', [
	'zeissApp.controllers',
	'angular-phoenix',
	'ui.bootstrap'
]).config(['PhoenixProvider', PhoenixProvider => {
  PhoenixProvider.setUrl('ws://machinestream.herokuapp.com/api/v1/events');
  // PhoenixProvider.setAutoJoin(false); // Phoenix will autojoin the socket unless this is called
}]);