'use strict';
angular.module('zeissApp.controllers', [])
.controller('machineListController', function($scope, $http, $uibModal, Phoenix) {
    $scope.machines = [];

    $http({
        url: "https://machinestream.herokuapp.com/api/v1/machines",
        method: "get"
    }).then(function successCallback(response) {
      $scope.machines = response.data.data;
      $scope.integrateSocket($scope);
      console.log(response.data.data);
    }, function errorCallback(response) {
      console.log("Error in list, status = "+response);
    });

    $scope.openModal = function(id) {
      var modalInstance = $uibModal.open({
          templateUrl: 'templates/modal.html',
          controller: 'machineSingleController',
          resolve: {
            id: function () {
              return id;
            }
          }
      });
    };

    $scope.integrateSocket = function($scope) {
      var chan = Phoenix.channel('events', {});
      // chan.join();

      // chan.join().promise
      //   .then(chann => {
      //     // Now our callbacks will get removed on scope destruction
      //     chann.on($scope, 'new', event => console.log(event))
      //     chann.on('message', hander)
      //   });
    };


}).controller('machineSingleController', function($scope, $http, $uibModalInstance, id) {
    $scope.machine = {};
    $scope.machineEvents = [];

    $http({
        url: "https://machinestream.herokuapp.com/api/v1/machines/" + id,
        method: "get"
    }).then(function successCallback(response) {
      $scope.machine = response.data.data;
      $scope.machineEvents = $scope.machine.events;
      console.log(response.data.data);
    }, function errorCallback(response) {
      console.log("Error in single, status = "+response);
    });

    $scope.ok = function() {
      $uibModalInstance.close();
    };
});